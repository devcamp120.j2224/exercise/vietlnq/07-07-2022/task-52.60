package com.devcamp.j53_basicjava;

public class Person extends Object {
    public String name;
    public int age;
    public double weight;
    public long salary;
    public String[] pets;

    // Constructor default: tên giống tên class, no param
    public Person() {
        this.name = "Viet Dep Trai";
        this.age = 28;
        this.weight = 30;
        this.salary = 10000000;
        this.pets = new String[] {"dog", "cat", "chicken"};
    }

    // Hàm khởi tạo có 3 tham số:
    public Person(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    // Hàm khởi tạo có 4 tham số:
    public Person(String name, int age, double weight, long salary) {
        // this.name = name;
        // this.age = age;
        // this.weight = weight;
        this(name, age, weight);
        this.salary = salary;
    }

    // Hàm khởi tạo có 4 tham số:
    public Person(String name, int age, double weight, String[] pets) {
        this(name, age, weight);
        this.pets = pets;
    }

    // Hàm khởi tạo có 5 tham số:
    public Person(String name, int age, double weight, long salary, String[] pets) {
        this(name, age, weight, salary);
        this.pets = pets;
    }

    public void showInfo() {
        System.out.println("Name: " + this.name);
        System.out.println("Age: " + this.age);
        System.out.println("Weight: " + this.weight);
        System.out.println("Salary: " + this.salary);
        System.out.println("Pets: " + this.pets);
    }

    // Override:
    public String toString() {
        String strPerson = "{";
        strPerson += "Name: " + this.name;
        strPerson += ", Age: " + this.age;
        strPerson += ", Weight: " + this.weight;
        strPerson += ", Salary: " + this.salary;
        strPerson += ", Pets: [";
        if (this.pets != null) {
            for (String pet: pets) {
                strPerson += pet;
            }
        }
        strPerson += "]}";

        return strPerson;
    }
}
