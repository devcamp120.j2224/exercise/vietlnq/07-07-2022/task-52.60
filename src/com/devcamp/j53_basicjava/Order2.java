package com.devcamp.j53_basicjava;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Order2 {
    int id;
    String customerName;
    Integer price;
    Date orderDate;
    Boolean confirm;
    String[] items;
    Person buyer;

    // 1.
    public Order2() {
        this.id = 1;
        this.buyer = new Person();
        this.customerName = this.buyer.name;
        this.price = 20000;
        this.orderDate = new Date();
        this.confirm = false;
        this.items = new String[] {"pizza", "nuoc ngot"};
    }

    // 2.
    public Order2(int id, Person buyer) {
        this.id = id;
        this.buyer = buyer;
        this.customerName = this.buyer.name;
    }

    // 3.
    public Order2(int id, Person buyer, Date orderDate) {
        this(id, buyer);
        this.orderDate = orderDate;
    }

    // 4.
    public Order2(int id, Integer price, Date orderDate, Boolean confirm, String[] items, Person buyer) {
        this(id, buyer, orderDate);
        this.items = items;
        this.price = price;
        this.confirm = confirm;
    }


    @Override
    public String toString() {
        String formattedDate = null;
        String formattedPrice = "N/A";

        if (this.orderDate != null) {
            Locale.setDefault(new Locale("vi", "VN"));
            String dateTimePattern = "dd-MMMM-yy HH:mm:ss.SSS";
            DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);
            
            formattedDate = defaulTimeFormatter.format(this.orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }

        if (this.price != null) {
            // Định dạng giá tiền:
            Locale usLocale = Locale.getDefault();
            NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

            formattedPrice = usNumberFormat.format(price);
        }

        String vResult = "Order [id= " + this.id
            + ", buyer= " + this.buyer
            + ", customerName= " + this.customerName
            + ", price=" + formattedPrice
            + ", orderDate=" + formattedDate
            + ", confirm= " + this.confirm
            + ", items= " + this.items;

        return vResult;
        
    }
}
