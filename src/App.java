import java.util.Date;
import java.util.ArrayList;

import com.devcamp.j53_basicjava.Order2;
import com.devcamp.j53_basicjava.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // ArrayList<Person> listPeople = new ArrayList<Person>();
        // Person person1 = new Person();
        

        // Person person2 = new Person("Tran My Linh", 28, 53);
        
        // Person person3 = new Person("Ho Ngoc Ha", 26, 78, 15000000);
       
        // Person person4 = new Person("Ho Hoai Anh", 26, 78, new String[] {"dog", "cat"});
        
        // Person person5 = new Person("Hong Dang", 26, 78, 17000000, new String[] {"dog", "cat"});

        // listPeople.add(person1);
        // listPeople.add(person2);
        // listPeople.add(person3);
        // listPeople.add(person4);
        // listPeople.add(person5);
        
        // for (Person person: listPeople) {
        //     System.out.println(person);
        // }
        ArrayList<Order2> listPeople2 = new ArrayList<Order2>();

        Order2 order1 = new Order2();
        listPeople2.add(order1);

        Order2 order2 = new Order2(2, new Person("Ho Hoai Anh", 45, 78));
        listPeople2.add(order2);

        Order2 order3 = new Order2(3, new Person("Hong Dang", 24, 54, 70000), new Date());
        listPeople2.add(order3);

        Order2 order4 = new Order2(4, 30000, new Date(), false,  new String[] {"Pizza", "Pho"}, new Person("Luu Huong Giang", 87, 67, new String[] {"dog", "cat"}));
        listPeople2.add(order4);

        for (Order2 order : listPeople2) {
            System.out.println(order.toString());
        }
    }
}
